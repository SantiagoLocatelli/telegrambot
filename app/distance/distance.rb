class Location
  attr_reader :latitude, :longitude

  def initialize(latitude, longitude)
    @latitude = latitude
    @longitude = longitude
  end
end

class Route
  attr_reader :from, :to, :transport, :departure_time, :duration

  def initialize(from, to, transport, departure_time, duration)
    @from = from
    @to = to
    @transport = transport
    @departure_time = departure_time
    @duration = Duration.new(duration)
  end

  def calculate_duration
    @duration.minutes
  end
end

class Duration
  SECONDS_PER_MINUTE = 60
  def initialize(time_in_seconds)
    @time = time_in_seconds
  end

  def minutes
    ((@time - Time.at(0)) / SECONDS_PER_MINUTE).ceil
  end
end
