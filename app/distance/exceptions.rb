class HereApiError < StandardError
  attr_reader :message

  def initialize
    super('Ocurrio un error en la llamada de la HereApi')
  end
end

class RouteError < StandardError
  attr_reader :message

  def initialize
    super('Ocurrio un error al crear la ruta')
  end
end
