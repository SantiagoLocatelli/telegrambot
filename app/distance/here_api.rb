require 'time'

class HereApi
  BASE_URL = 'https://router.hereapi.com'.freeze

  # rubocop:disable Metrics/AbcSize
  def self.get_route(api_key, from, to, transport)
    connection = Faraday::Connection.new BASE_URL

    origin = "#{from.latitude},#{from.longitude}"
    destination = "#{to.latitude},#{to.longitude}"
    routes_url = "/v8/routes?apiKey=#{api_key}&transportMode=#{transport}&origin=#{origin}&destination=#{destination}&return=summary"
    response = connection.get routes_url
    if response.success?
      begin
        json_response = JSON.parse(response.body)
        first_section = json_response['routes'][0]['sections'][0]
        departure_time = first_section['departure']['time']
        duration = first_section['summary']['duration']
        Route.new(from, to, transport, Time.parse(departure_time), Time.at(duration))
      rescue StandardError
        raise RouteError
      end
    else
      puts "RESPONSE: #{response.status}"
      raise HereApiError
    end
  end
  # rubocop:enable Metrics/AbcSize
end
