require "#{File.dirname(__FILE__)}/../lib/routing"
require "#{File.dirname(__FILE__)}/../lib/version"
require "#{File.dirname(__FILE__)}/tv/series"
require "#{File.dirname(__FILE__)}/distance/distance"
require "#{File.dirname(__FILE__)}/distance/here_api"
require "#{File.dirname(__FILE__)}/distance/exceptions"

class Routes
  include Routing

  on_message '/start' do |bot, message|
    bot.api.send_message(chat_id: message.chat.id, text: "Hola, #{message.from.first_name}")
  end

  on_message_pattern %r{/say_hi (?<name>.*)} do |bot, message, args|
    bot.api.send_message(chat_id: message.chat.id, text: "Hola, #{args['name']}")
  end

  on_message '/stop' do |bot, message|
    bot.api.send_message(chat_id: message.chat.id, text: "Chau, #{message.from.username}")
  end

  on_message '/time' do |bot, message|
    bot.api.send_message(chat_id: message.chat.id, text: "La hora es, #{Time.now}")
  end

  on_message '/tv' do |bot, message|
    kb = [Tv::Series.all.map do |tv_serie|
      Telegram::Bot::Types::InlineKeyboardButton.new(text: tv_serie.name, callback_data: tv_serie.id.to_s)
    end]
    markup = Telegram::Bot::Types::InlineKeyboardMarkup.new(inline_keyboard: kb)

    bot.api.send_message(chat_id: message.chat.id, text: 'Quien se queda con el trono?', reply_markup: markup)
  end

  on_message '/busqueda_centro' do |bot, message|
    kb = [
      Telegram::Bot::Types::KeyboardButton.new(text: 'Compartime tu ubicacion', request_location: true)
    ]
    markup = Telegram::Bot::Types::ReplyKeyboardMarkup.new(keyboard: kb)
    bot.api.send_message(chat_id: message.chat.id, text: 'Busqueda por ubicacion', reply_markup: markup)
  end

  on_location_response do |bot, message|
    response = "Ubicacion es Lat:#{message.location.latitude} - Long:#{message.location.longitude}"
    puts response
    bot.api.send_message(chat_id: message.chat.id, text: response)
  end

  on_response_to 'Quien se queda con el trono?' do |bot, message|
    response = Tv::Series.handle_response message.data
    bot.api.send_message(chat_id: message.message.chat.id, text: response)
  end

  on_message '/version' do |bot, message|
    bot.api.send_message(chat_id: message.chat.id, text: Version.current)
  end

  default do |bot, message|
    bot.api.send_message(chat_id: message.chat.id, text: 'Uh? No te entiendo! Me repetis la pregunta?')
  end

  on_message_pattern %r{/viaje (?<lat1>.*),(?<lng1>.*),(?<lat2>.*),(?<lng2>.*)} do |bot, message, args|
    if args['lat1'].nil? || args['lng1'].nil? || args['lat2'].nil? || args['lng2'].nil?
      bot.api.send_message(chat_id: message.chat.id, text: 'Faltan latitud y longitud de las ubicaciones')
    else
      from = Location.new(args['lat1'], args['lng1'])
      to = Location.new(args['lat2'], args['lng2'])
      transport_mode = 'car'
      begin
        route = HereApi.get_route(ENV['HERE_API_KEY'], from, to, transport_mode)
        duration = route.calculate_duration
        bot.api.send_message(chat_id: message.chat.id, text: "Vas a tardar #{duration} minutos en hacer tu viaje en auto")
      rescue RouteError
        bot.api.send_message(chat_id: message.chat.id, text: 'Latitud o Longitud invalida')
      rescue HereApi
        bot.api.send_message(chat_id: message.chat.id, text: 'Ocurrio un error al calcular la duracion')
      end
    end
  end
end
