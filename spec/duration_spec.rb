require 'time'

describe Duration do
  it 'should get 0 minutes' do
    seconds = 0
    minutes_expected = 0
    duration = described_class.new(Time.at(seconds))
    expect(duration.minutes).to eq(minutes_expected)
  end

  it 'should get 1 minutes' do
    seconds = 30
    minutes_expected = 1
    duration = described_class.new(Time.at(seconds))
    expect(duration.minutes).to eq(minutes_expected)
  end

  it 'should get 5 minutes' do
    seconds = 300
    minutes_expected = 5
    duration = described_class.new(Time.at(seconds))
    expect(duration.minutes).to eq(minutes_expected)
  end
end
