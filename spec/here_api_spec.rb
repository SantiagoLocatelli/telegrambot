describe HereApi do
  it 'here api response status OK' do
    WebMock.allow_net_connect!
    from = Location.new('42.532914', '13.399983')
    to = Location.new('42.532914', '13.399983')
    response = described_class.get_route('OOqo2aNNjf2eIqtRpERSbVnCzCmr1fMiE8utQTj2Xl0', from, to, 'car')
    expect(response.calculate_duration).to eq(0)
  end
end
