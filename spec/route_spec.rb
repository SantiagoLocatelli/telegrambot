require 'time'

describe Route do
  # rubocop:disable RSpec/ExampleLength
  it 'should get 0 minutes as duration' do
    from = Location.new('42.532914', '13.399983')
    to = Location.new('42.532914', '13.399983')
    transport = 'car'
    departure_time = '2023-11-01 22:35:40'
    duration = Time.at(0)
    minutes_expected = 0
    route = described_class.new(from, to, transport, departure_time, duration)
    expect(route.calculate_duration).to eq(minutes_expected)
  end

  it 'should get 10 minutes as duration' do
    from = Location.new('42.532914', '13.399983')
    to = Location.new('42.032914', '13.399983')
    transport = 'car'
    departure_time = '2023-11-01 22:35:40'
    duration = Time.at(600)
    minutes_expected = 10
    route = described_class.new(from, to, transport, departure_time, duration)
    expect(route.calculate_duration).to eq(minutes_expected)
  end
  # rubocop:enable RSpec/ExampleLength
end
